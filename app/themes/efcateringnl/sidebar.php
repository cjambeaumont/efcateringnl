		<div id="sidebar" class="sidebar clearfix" role="complementary">

			<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar' ); ?>

			<?php endif; ?>

		</div>