<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

	<?php while (have_posts()) : the_post(); ?>

	<div class="row clearfix">

		<nav role="navigation" class="navigation">

			<a href="<?php echo home_url(); ?>" rel="nofollow">
				<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-small.png" alt="E/F Catering">
			</a>

			<ul>
			<?php while ( have_rows('home_cb') ) : the_row(); ?>

				<?php if( get_row_layout() == 'home_cb_full_width' ): ?>
					<?php $title = get_sub_field('home_cb_full_width_title'); ?>
					<li>
						<a href="#<?php echo sanitize_title($title); ?>"><?php echo $title; ?></a>
					</li>
				<?php endif; ?>

			<?php endwhile; ?>
			</ul>

		</nav>

		<div class="content">

		<?php while ( have_rows('home_cb') ) : the_row(); ?>

			<?php if( get_row_layout() == 'home_cb_full_width' ): ?>

			<?php $title = get_sub_field('home_cb_full_width_title'); ?>

			<article id="<?php echo sanitize_title($title); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

				<header class="article-header">
					<h1 class="page-title"><?php echo $title; ?></h1>
				</header>

				<section class="entry-content clearfix" itemprop="articleBody">
					<?php the_sub_field('home_cb_full_width_content') ?>
				</section>

			</article>

			<?php endif; ?>

		<?php endwhile; ?>

		</div>

		<div class="address">

			<?php get_sidebar(); ?>

		</div>

	</div>

	<?php endwhile; ?>

<?php get_footer(); ?>
