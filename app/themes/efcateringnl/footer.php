	<footer class="footer" role="contentinfo">

		<div class="footer-inner clearfix">

			<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <strong><?php bloginfo( 'name' ); ?></strong>.</p>

		</div>

	</footer>

</div>

<?php wp_footer(); ?>

</body>

</html>
