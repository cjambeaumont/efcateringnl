// @codekit-prepend "jquery.scrolltofixed.js"
// @codekit-prepend "jquery.scrollto.js"
// @codekit-prepend "jquery.localscroll.js"

jQuery(document).ready(function($) {

	jQuery.extend( jQuery.easing, {
        def: 'easeInOutQuart',
        easeInOutQuart: function (x, t, b, c, d) {
            if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
            return -c/2 * ((t-=2)*t*t*t - 2) + b;
        }
    });	
 
	$('.navigation, .address').scrollToFixed({ marginTop: 32 });

	$('.navigation').localScroll({
		axis: 'y',
		hash: true,
		easing: 'easeInOutQuart',
		duration: 500,
		offset: {
			top: 0
		}
	});

});